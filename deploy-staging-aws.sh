#!/bin/sh
echo "Install package"
#npm install --production

echo "Build project"
#npm run build

echo "Copy build folder to server"
scp -r -i ~/Dropbox/aws/vncss/vncss-camera-ec2.pem /home/ledinhduy/Code/dz/outsource/vncss/frontend/src ubuntu@18.136.107.70:/home/ubuntu/Code/vncssCamera/frontend
scp -r -i ~/Dropbox/aws/vncss/vncss-camera-ec2.pem /home/ledinhduy/Code/dz/outsource/vncss/frontend/public ubuntu@18.136.107.70:/home/ubuntu/Code/vncssCamera/frontend
scp -r -i ~/Dropbox/aws/vncss/vncss-camera-ec2.pem /home/ledinhduy/Code/dz/outsource/vncss/frontend/.env ubuntu@18.136.107.70:/home/ubuntu/Code/vncssCamera/frontend
scp -r -i ~/Dropbox/aws/vncss/vncss-camera-ec2.pem /home/ledinhduy/Code/dz/outsource/vncss/frontend/package.json ubuntu@18.136.107.70:/home/ubuntu/Code/vncssCamera/frontend

echo "Login to server"
ssh -i ~/Dropbox/aws/vncss/vncss-camera-ec2.pem ubuntu@18.136.107.70

# pm2 serve --port=3000 --name=vncss_frontend --spa

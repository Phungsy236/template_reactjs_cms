import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { PAGE_SIZE } from '../app/constant/config';
import { _convertObjectToQuery } from '../app/utils/apiService';

export const GetImages = createAsyncThunk(
  'imageSlice/GetImage',
  (payload, { rejectWithValue }) => {
    return new Promise((resolve, reject) => {
      axios({
        url: `${
          process.env.REACT_APP_BACKEND_URL
        }/images/?${_convertObjectToQuery(payload)}`,
        method: 'GET',
        headers: {
          // 'Content-Type': 'application/json'
        }
      })
        .then(res => resolve(res.data.payload))
        .catch(err => {
          if (!err.response) reject(err);
          reject(rejectWithValue(err.response?.data));
        });
    });
  }
);

export const imageSlice = createSlice({
  name: 'imageSlice',
  initialState: {
    err: null,
    isLoading: false,
    currentPageId: 1,
    currentImageId: 1,
    totalPage: 0,
    listImage: null
  },
  reducers: {
    setPageId: (state, action) => {
      state.currentPageId = action.payload;
    },
    setImageId: (state, action) => {
      state.currentImageId = action.payload;
    }
  },
  extraReducers: {
    [GetImages.pending]: (state, action) => {
      state.isLoading = true;
      state.err = null;
    },
    [GetImages.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.listImage = action.payload.images;
      let totalImage = action.payload.numberOfItem;
      state.totalPage =
        totalImage % PAGE_SIZE === 0
          ? totalImage / PAGE_SIZE
          : Math.ceil(totalImage / PAGE_SIZE);
    },
    [GetImages.rejected]: (state, action) => {
      state.isLoading = false;
      state.err = 'Có lỗi xảy ra , không tải được ảnh ';
    }
  }
});
export const { setImageId, setPageId } = imageSlice.actions;

export default imageSlice.reducer;

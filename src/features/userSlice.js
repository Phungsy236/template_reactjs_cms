import { createSlice } from '@reduxjs/toolkit';
import { HTTP_GETTYPE, STATUS_API } from 'src/app/constant/config';
import AxiosAdapter from './AxiosAdapter';

export const getListAgency = AxiosAdapter.GetHttp(
  'userSlice/GetListAgency',
  '/agencies/member',
  HTTP_GETTYPE.ALL
);

// export const getDetailUser = AxiosAdapter.GetHttp(
//   'userSlice/GetDetailUser',
//   '/users/',
//   HTTP_GETTYPE.DETAIL
// );
export const userSlice = createSlice({
  name: 'userSlice',
  initialState: {
    listAgency: [
      {
        id: 1,
        accName: 'vtshongkong',
        name: 'abc111',
        accType: 'quản trị',
        latestLogin: '17-06-2021',
        inforLogin: 'linux asus u510x'
      },
      {
        id: 1,
        accName: 'vtshongkong',
        name: 'abc111',
        accType: 'quản trị',
        latestLogin: '17-06-2021',
        inforLogin: 'linux asus u510x'
      },
      {
        id: 1,
        accName: 'vtshongkong',
        name: 'abc111',
        accType: 'quản trị',
        latestLogin: '17-06-2021',
        inforLogin: 'linux asus u510x'
      },
      {
        id: 1,
        accName: 'vtshongkong',
        name: 'abc111',
        accType: 'quản trị',
        latestLogin: '17-06-2021',
        inforLogin: 'linux asus u510x'
      }
    ],
    totalAgency: 0,

    errorGetAll: null,
    statusGetAll: null,

    err: null,
    detailUser: null,
    statusGetDetail: null
  },
  reducers: {
    resetChange: state => {
      state.statusGetAll = null;
      state.statusGetDetail = null;
    }
  },
  extraReducers: {
    [getListAgency.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetAll = null;
      state.listAgency = null;
    },
    [getListAgency.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      console.log(action);
      state.listAgency = action.payload.users;
      // state.totalUser = action.payload.payload.numberOfItem;
    },
    [getListAgency.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetAll = action.payload?.message || action.error;
    }
  }
});
export const { resetChange } = userSlice.actions;

export default userSlice.reducer;

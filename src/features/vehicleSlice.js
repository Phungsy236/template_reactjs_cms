import { createSlice } from '@reduxjs/toolkit';
import { HTTP_GETTYPE, STATUS_API } from 'src/app/constant/config';
import AxiosAdapter from './AxiosAdapter';

export const getListVehicle = AxiosAdapter.GetHttp(
  'vehicleSlice/GetListVehicles',
  '/vehicles',
  HTTP_GETTYPE.ALL_PAGINATION
);

export const getDetailVehicle = AxiosAdapter.GetHttp(
  'vehicleSlice/GetDetailVehicle',
  '/vehicles/',
  HTTP_GETTYPE.DETAIL
);

export const getVehicleByOwnerId = AxiosAdapter.GetHttp(
  'vehicleSlice/GetVehicleByOwnerId',
  '/vehicles/users/',
  HTTP_GETTYPE.DETAIL
)

export const vehicleSlice = createSlice({
  name: 'vehicleSlice',
  initialState: {
    listVehicle: null,
    totalVehicle: 0,
    errorGetList: null,
    statusGetAll: null,

    errorGetVehicleByOwnerId: null,
    statusGetVehicleByOwnerId: null,

    err: null,
    detailVehicle: null,
    statusGetDetail: null,
    userSelected: null,
    selectedLicensePlate: null,
    existLincesePlate: true,
  },



  reducers: {
    setUserSelected: (state, action) => {
      state.userSelected = action.payload;
    },
    setSelectedLicensePlate: (state, action) => {
      state.selectedLicensePlate = action.payload;
    },
    setExistLincesePlate: (state, action) => {
      state.selectedLicensePlate = action.payload;

    },
    resetChange: state => {
      state.statusGetAll = null;
      state.statusGetDetail = null;
      state.userSelected = null;
      state.selectedLicensePlate = null;
    }
  },
  extraReducers: {
    [getListVehicle.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.listVehicle = null;
      state.totalVehicle = 0;
    },
    [getListVehicle.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.listVehicle = action.payload.payload.vehicles;
      state.totalVehicle = action.payload.payload.numberOfItem;
    },
    [getListVehicle.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList = action.payload?.message || action.error;
    },
    [getDetailVehicle.pending]: state => {
      state.statusGetDetail = STATUS_API.PENDING;
      state.err = null;
      state.detailVehicle = null;
    },
    [getDetailVehicle.fulfilled]: (state, action) => {
      state.statusGetDetail = STATUS_API.SUCCESS;
      state.detailVehicle = action.payload.payload.vehicle;
    },
    [getDetailVehicle.rejected]: (state, action) => {
      state.statusGetDetail = STATUS_API.ERROR;
      state.errorGetList = action.payload?.message || action.error;
    },
    [getVehicleByOwnerId.pending]: state => {
      state.statusGetVehicleByOwnerId = STATUS_API.PENDING;
      state.errorGetVehicleByOwnerId = null;
      state.listVehicle = [];
    },
    [getVehicleByOwnerId.fulfilled]: (state, action) => {
      state.statusGetVehicleByOwnerId = STATUS_API.SUCCESS;
      state.errorGetVehicleByOwnerId = null;
      state.listVehicle = action.payload.payload.vehicles;
    },
    [getVehicleByOwnerId.rejected]: (state, action) => {
      state.statusGetVehicleByOwnerId = STATUS_API.ERROR;
      state.errorGetVehicleByOwnerId = action.payload?.message || action.error;
      state.listVehicle = [];
    }
  }
});
export const { resetChange,   setUserSelected, setSelectedLicensePlate, setExistLincesePlate } = vehicleSlice.actions;

export default vehicleSlice.reducer;

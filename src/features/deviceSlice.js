import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
  CREATE_DEVICE_STEP,
  HTTP_GETTYPE, MSG_TIMEOUT_REQUEST,
  STATUS_API, TIMEOUT_DEFAULT
} from 'src/app/constant/config';
import AxiosAdapter from './AxiosAdapter';
import axios from 'axios';
import Cookie from 'js-cookie';
import * as positions from '../factory/positions.json'

export const getListDevice = AxiosAdapter.GetHttp(
  'deviceSlice/GetListDevices',
  '/devices',
  HTTP_GETTYPE.ALL_PAGINATION
);
export const activeDevice = AxiosAdapter.HttpPost(
  'deviceSlice/activeSlice',
  '/devices/active'
);
export const getDetailDevice = AxiosAdapter.GetHttp(
  'deviceSlice/GetDetailDevice',
  '/devices/',
  HTTP_GETTYPE.DETAIL
);

export const updateDevice = AxiosAdapter.HttpPut(
  'deviceSlice/updateDevice',
  '/devices/',
)

export const getPositionDevice = AxiosAdapter.GetHttp(
  'deviceSlice/GetPositionDevices',
  '/devices/positions'
);

export const deleteDevice = createAsyncThunk(
  'deviceSlice/DeleteDevice',
  (payload, { rejectWithValue }) => {
    return new Promise((resolve, reject) => {
      axios({
        url: `${process.env.REACT_APP_BACKEND_URL}/devices/${payload.id}`,
        method: 'Delete',
        headers: {
          'Access-Control-Allow-Origin': true,
          'Content-Type': 'application/json; charset=utf-8',
          Authorization: 'Bearer ' + Cookie.get('access-token'),
          timeout: TIMEOUT_DEFAULT
        }
      })
        .then(res => resolve(res.data))
        .catch(err => {
          if (err.code === 'ECONNABORTED') reject(MSG_TIMEOUT_REQUEST);
          if (!err.response) reject(err);
          reject(rejectWithValue(err.response?.data));
        });
    });
  });

export const deviceSlice = createSlice({
  name: 'deviceSlice',
  initialState: {
    listDevice: [],
    totalDevice: 0,
    errorGetList: null,
    statusGetAll: null,
    statusGetPositions: null,
    statusCreate: null,
    statusUpdate: null,
    statusDelete: null,
    listPositions: positions.positions,

    err: null,
    detailDevice: null,
    statusGetDetail: null,

    activeStep: CREATE_DEVICE_STEP.ADD_INFO_DEVICE,
    objectCreating: {
      id: '',
      name: '',
      status: 'active',
      sim: '',
      mfg: '',
      device_type_id: '',
      serial: '',
      vehicle_license_plate_exists: '',
      vehicle_license_plate: '',
      customer_id: ''
    },

  },
  reducers: {
    resetChange: state => {
      state.statusGetAll = null;
      state.statusGetDetail = null;
    },
    setActiveStep: (state, action) => {
      state.activeStep = action.payload;
    },
    setObjectCreating: (state, action) => {
      state.objectCreating = action.payload;
    },
    resetObjectCreating: state => {
      state.objectCreating = {
        name: '',
        status: 'active',
        sim: '',
        mfg: '',
        device_type_id: '',
        serial: '',
        vehicle_license_plate_exists: '',
        vehicle_license_plate: '',
        customer_id: 0
      };
    }
  },
  extraReducers: {
    [getListDevice.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.listDevice = null;
      state.totalDevice = 0;
    },
    [getListDevice.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.listDevice = action.payload.payload.devices;
      state.totalDevice = action.payload.payload.numberOfItem;
    },
    [getListDevice.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList = action.payload?.message || action.error;
    },
    [getDetailDevice.pending]: state => {
      state.statusGetDetail = STATUS_API.PENDING;
      state.err = null;
      state.detailDevice = null;
    },
    [getDetailDevice.fulfilled]: (state, action) => {
      state.statusGetDetail = STATUS_API.SUCCESS;
      state.detailDevice = action.payload.payload.device;
    },
    [getDetailDevice.rejected]: (state, action) => {
      state.statusGetDetail = STATUS_API.ERROR;
      state.errorGetList = action.payload?.message || action.error;
    },
    [getDetailDevice.pending]: state => {
      state.statusGetPositions = STATUS_API.PENDING;
      state.err = null;
      state.listPositions = null;
    },
    [getDetailDevice.fulfilled]: (state, action) => {
      state.statusGetPositions = STATUS_API.SUCCESS;
      state.listPositions = action.payload.payload.positions;
    },
    [getDetailDevice.rejected]: (state, action) => {
      state.statusGetPositions = STATUS_API.ERROR;
      state.errorGetList = action.payload?.message || action.error;
    },
    [activeDevice.pending]: state => {
      state.statusCreate = STATUS_API.PENDING;
      state.err = null;
    },
    [activeDevice.fulfilled]: (state, action) => {
      state.statusCreate = STATUS_API.SUCCESS;
      // state.detailDevice = action.payload.payload.device;
    },
    [activeDevice.rejected]: (state, action) => {
      state.statusCreate = STATUS_API.ERROR;
      state.err = action.payload?.message || action.error;
    },
    [deleteDevice.pending]: state => {
      state.statusDelete = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteDevice.fulfilled]: (state, action) => {
      state.statusDelete = STATUS_API.SUCCESS;
      state.err = null;
    },
    [deleteDevice.rejected]: (state, action) => {
      state.statusDelete = STATUS_API.ERROR;
      state.err = action.payload?.message || action.error;
    },
    [updateDevice.pending]: state => {
      state.statusUpdate = STATUS_API.PENDING;
      state.err = null;
    },
    [updateDevice.fulfilled]: (state, action) => {
      state.statusUpdate = STATUS_API.SUCCESS;
      state.err = null;
    },
    [updateDevice.rejected]: (state, action) => {
      state.statusUpdate = STATUS_API.ERROR;
      state.err = action.payload?.message || action.error;
    }
  }
});
export const {
  resetChange,
  setActiveStep,
  setObjectCreating,
  resetObjectCreating
} = deviceSlice.actions;

export default deviceSlice.reducer;

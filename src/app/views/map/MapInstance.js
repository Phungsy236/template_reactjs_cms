import React from 'react';
import { GoogleMap, useJsApiLoader} from '@react-google-maps/api';
import { Marker } from '@react-google-maps/api';

import {
  VEHICLE_STATUS
} from 'src/app/constant/config';

const containerStyle = {
  // width: '100vw',
  height: '100vh'
};

const center = {
  lat: -3.745,
  lng: -38.523
};

const renderIconCar = (status) => {
  const baseUrlIcon = "/static/images/map/"

  switch (status) {
    case VEHICLE_STATUS.ACTIVE: 

      return  `${baseUrlIcon}green_car.png`
    default: 

      return  `${baseUrlIcon}gray_car.png`
  }
}

function Map({ positions }) {
  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: process.env['REACT_APP_GGMAP_API_KEY'],
  })

  const renderGoogleMap = () => {
    return <GoogleMap mapContainerStyle={containerStyle} center={positions[1]} zoom={10}>
      {positions ? positions.map((position, index) => {
        console.log(position)
        return (
          <Marker 
            key={index}
            position={position}
            icon={{
              url: renderIconCar(position.status),
              scaledSize: new window.google.maps.Size(40, 20)
            }}
          />)
      }): ''}
    </GoogleMap>
  }
  return  isLoaded ? renderGoogleMap() : null
}

export default React.memo(Map);

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Card, Container, makeStyles } from '@material-ui/core';
import Page from 'src/app/components/Page';
import Map from './MapInstance';
import { getPositionDevice } from 'src/features/deviceSlice'

const MapView = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(getPositionDevice());
  },[])

  const positions = useSelector(state => state.deviceSlice.listPositions);

  return (
    <Page className={classes.root} title="Device">
      <Container maxWidth={false}>
        <Card className={classes.mapWrap}>
          <Box>
            <Map positions={positions} />
          </Box>
        </Card>
      </Container>
    </Page>
  );
};
const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  mapWrap: {
    padding: 10,
    width: '100%',
    height: '100%'
  }
}));

export default MapView;

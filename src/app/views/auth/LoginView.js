import React, { useEffect, useState } from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { Formik } from 'formik';
import {
  Box,
  Button,
  CircularProgress,
  Container,
  Link,
  makeStyles,
  TextField,
  Typography
} from '@material-ui/core';
import Page from 'src/app/components/Page';
import { clearMassageErrorAction, Login } from '../../../features/authSlice';
import { DRAFT_USER } from 'src/app/constant/config';
import { useDispatch, useSelector } from 'react-redux';
import MsgError from '../../components/MsgError';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const LoginView = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const isLoadingLogin_gs = useSelector(
    state => state.authSlice.isLoadingLogin
  );
  const failStatus_gs = useSelector(state => state.authSlice.failStatus);
  const dataLogin = useSelector(state => state.authSlice.dataLogin);
  const isLogin_gs = useSelector(state => state.authSlice.isLogin);
  const [isHiddenMsg_ls, setHiddenMsgStatus] = useState(false);
  const dispatch = useDispatch();
  useEffect(() => {
    if (isLogin_gs) {
      if (dataLogin && dataLogin.is_delete === DRAFT_USER.id) {
        navigate('/waiting-confirm', { replace: true });
      } else navigate('/app/dashboard', { replace: true });
    }
  }, [isLogin_gs, navigate]);
  return (
    <Page className={classes.root} title="Đăng nhập">
      <Box
        display="flex"
        flexDirection="column"
        height="100%"
        justifyContent="center"
      >
        <Container maxWidth="sm">
          <Formik
            initialValues={{
              email: '',
              password: ''
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string()
                .email('Email chưa đúng định dạng ')
                .max(255)
                .required('Bạn cần nhập email'),
              password: Yup.string()
                .max(255)
                .required('Bạn cần nhập mật khẩu')
            })}
            onSubmit={values => {
              dispatch(clearMassageErrorAction());
              setHiddenMsgStatus(false);
              dispatch(Login(values));
            }}
          >
            {({
              errors,
              handleBlur,
              handleChange,
              handleSubmit,
              isSubmitting,
              touched,
              values
            }) => (
              <form onSubmit={handleSubmit}>
                <Box mb={3}>
                  <Typography color="textPrimary" variant="h1">
                    Trang đăng nhập
                  </Typography>
                  <br />
                  <Typography
                    color="textSecondary"
                    gutterBottom
                    variant="body2"
                  >
                    Xin chào đến với Vncss admin
                  </Typography>
                </Box>
                <TextField
                  error={Boolean(touched.email && errors.email)}
                  fullWidth
                  helperText={touched.email && errors.email}
                  label="Địa chỉ email"
                  margin="normal"
                  name="email"
                  onBlur={e => {
                    handleBlur(e);
                    setHiddenMsgStatus(true);
                  }}
                  onChange={e => {
                    handleChange(e);
                    setHiddenMsgStatus(true);
                  }}
                  type="text"
                  value={values.email}
                  variant="outlined"
                />
                <TextField
                  error={Boolean(touched.password && errors.password)}
                  fullWidth
                  helperText={touched.password && errors.password}
                  label="Mật khẩu"
                  margin="normal"
                  name="password"
                  onBlur={e => {
                    handleBlur(e);
                    setHiddenMsgStatus(true);
                  }}
                  onChange={e => {
                    handleChange(e);
                    setHiddenMsgStatus(true);
                  }}
                  type="password"
                  value={values.password}
                  variant="outlined"
                />
                <Typography
                  color={'error'}
                  variant={'subtitle1'}
                  align={'center'}
                >
                  {!isHiddenMsg_ls && failStatus_gs && (
                    <MsgError content={failStatus_gs.message} />
                  )}
                </Typography>
                <Box my={2}>
                  <Button
                    color="primary"
                    disabled={isLoadingLogin_gs}
                    fullWidth
                    size="large"
                    type="submit"
                    variant="contained"
                  >
                    Đăng nhập
                    {isLoadingLogin_gs && (
                      <div style={{ marginLeft: 20 }}>
                        <CircularProgress color={'secondary'} size={20} />
                      </div>
                    )}
                  </Button>
                </Box>

                <Typography color="textSecondary" variant="body1">
                  Chưa có tài khoản{' '}
                  <Link
                    component={RouterLink}
                    to={'/register'}
                    variant="h6"
                    onClick={() => setHiddenMsgStatus(true)}
                  >
                    Đăng ký !
                  </Link>
                </Typography>
              </form>
            )}
          </Formik>
        </Container>
      </Box>
    </Page>
  );
};

export default LoginView;

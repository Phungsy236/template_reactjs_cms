import React, { useEffect, useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Box,
  Card,
  Chip,
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Tooltip,
  Typography
} from '@material-ui/core';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import CloseIcon from '@material-ui/icons/Close';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import DoneIcon from '@material-ui/icons/Done';
import VisibilityIcon from '@material-ui/icons/Visibility';
import moment from 'moment';
import {
  ACTION_TABLE,
  messageToastType_const,
  PAGE_SIZE_LIST,
  STATUS_API,
  DEVICE_STATUS,
  DEVICE_STATUS_VALUE
} from 'src/app/constant/config';
import LoadingComponent from 'src/app/components/Loading';
import { useDispatch, useSelector } from 'react-redux';
import { Key } from 'react-feather';

const Results = ({
                   className,
                   listDevice,
                   isLoading,
                   getListDeviceRef,
                   actionDetailsDeviceRef,
                   totalDevice,
                   actionDeleteDeviceRef,
//   checkPermissionEdit,
//   checkPermissionView,
                   ...rest
                 }) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [limit, setLimit] = useState(PAGE_SIZE_LIST);
  const [page, setPage] = useState(0);
  const [params, setParams] = useState({
    page: page,
    pageSize: limit
  });
  const getStatusDevice = status => {
    let content = '';
    const index = DEVICE_STATUS_VALUE.findIndex(st => st.id === status);
    if (index >= 0) content = DEVICE_STATUS_VALUE[index]?.title;
    return <Chip label={content} color='primary' clickable />;
  };

  const handleLimitChange = event => {
    setLimit(event.target.value);
    if (!getListDeviceRef) return;
    const newparams = Object.assign({}, params, {
      pageSize: event.target.value,
      page: 1
    });
    setParams(newparams);
    getListDeviceRef(newparams);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
    if (!getListDeviceRef) return;
    const newparams = Object.assign({}, params, { page: newPage + 1 });
    setParams(newparams);
    getListDeviceRef(newparams);
  };

  const onEditDevice = (type, device) => {
    if (!actionDetailsDeviceRef) return;
    const sendData = { type: type, data: device };
    actionDetailsDeviceRef(sendData);
  };

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      {isLoading ? (
        <LoadingComponent />
      ) : (
        <div>
          <PerfectScrollbar>
            <Box minWidth={1050}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>STT</TableCell>
                    <TableCell>Tên</TableCell>
                    <TableCell>Loại</TableCell>
                    <TableCell>Serial</TableCell>
                    {/*<TableCell>Phiên bản phần mềm</TableCell>*/}
                    {/*<TableCell>Phiên bản phần cứng</TableCell>*/}
                    <TableCell>Ngày kích hoạt</TableCell>
                    {/*<TableCell>Trạng thái</TableCell>*/}
                    <TableCell>Thao tác</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {listDevice && listDevice.map((device, index) => (
                    <TableRow
                      hover
                      key={device.id}
                    >
                      <TableCell>{ (page * params.pageSize) + index + 1}</TableCell>
                      <TableCell>{device.name}</TableCell>
                      <TableCell>{device.deviceType?.name}</TableCell>
                      <TableCell className={classes.minWithColumn}>{device.serial}</TableCell>
                      {/*<TableCell>{device.firmware_version}</TableCell>*/}
                      {/*<TableCell>{device.hardware_version}</TableCell>*/}
                      <TableCell>{moment(device.created_at).format('DD-MM-yyyy')}</TableCell>
                      {/*<TableCell>{getStatusDevice(device.status)}</TableCell>*/}
                      <TableCell>
                        <div className={classes.groupAction}>
                          {/*<div*/}
                          {/*className="action"*/}
                          {/*  onClick={() =>*/}
                          {/*    onEditDevice(ACTION_TABLE.PREVIEW, device)*/}
                          {/*  }*/}
                          {/*>*/}
                          {/*  <Tooltip title="Chi tiết">*/}
                          {/*    <VisibilityIcon />*/}
                          {/*  </Tooltip>*/}
                          {/*</div>*/}
                          <div
                            className="action"
                            onClick={() => onEditDevice(ACTION_TABLE.EDIT, device)}
                          >
                           <Tooltip title="Chỉnh sửa">
                              <EditIcon />
                            </Tooltip>
                          </div>
                          <div
                            className="action"
                            onClick={() => actionDeleteDeviceRef(device)}
                          >
                            <Tooltip title="Xóa">
                              <DeleteIcon />
                            </Tooltip>
                          </div>
                        </div>

                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </PerfectScrollbar>
          <TablePagination
            component="div"
            count={totalDevice}
            onChangePage={handlePageChange}
            onChangeRowsPerPage={handleLimitChange}
            page={page}
            rowsPerPage={limit}
            rowsPerPageOptions={[5, 10, 25]}
          />
        </div>
      )}
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  listDevice: PropTypes.array.isRequired
};

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  groupAction: {
    display: 'flex',
    alignItems: 'center',
    '& .action': {
      cursor: 'pointer',
      padding: '0 5px',
      '&:hover': {
        color: '#3f51b5'
      }
    }
  },
  minWithColumn: {
    minWidth: '150px'
  }
}));

export default Results;

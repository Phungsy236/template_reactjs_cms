import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Box, Container, makeStyles } from '@material-ui/core';
import {
  PAGE_SIZE_LIST,
  ACTION_TABLE,
  STATUS_API,
  messageToastType_const,
  CREATE_DEVICE_STEP
} from 'src/app/constant/config';
import ToastMessage from 'src/app/components/ToastMessage';
import DialogConfirm from 'src/app/components/DialogConfirm';
import Page from 'src/app/components/Page';
import {
  closeDialogConfirm,
  showDialogConfirm,
  showToast
} from 'src/features/uiSlice';
import Results from './Results';
import Toolbar from './ToolBar';
import DetailsDevice from './device_details/index';
import { MESSAGE } from 'src/app/constant/message';
import { resetChange } from 'src/features/vehicleSlice';
import {
  resetObjectCreating,
  setActiveStep,
} from '../../../features/deviceSlice';
import {getListDevice, deleteDevice} from 'src/features/deviceSlice'
import { finishRegisterAction } from '../../../features/authSlice';

const DeviceListView = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  // get data api
  const listDevice = useSelector(state => state.deviceSlice.listDevice);
  const totalDevice = useSelector(state => state.deviceSlice.totalDevice);
  const isLoading = useSelector(state => state.deviceSlice.isLoading);
  const statusCreate = useSelector(state => state.deviceSlice.statusCreate);
  const statusDelete = useSelector(state => state.deviceSlice.statusDelete);
  const statusUpdate = useSelector(state => state.deviceSlice.statusUpdate);

  const dataLogin = useSelector(state => state.authSlice.dataLogin);

  // set state
  const [isShowModalDeviceDetails, setIsShowModalDeviceDetails] = useState(
    false
  );
  const [isDeleted, setIsDeleted] = useState(false);
  const [deleteItem, setDeleteItem] = useState(null);

  const [sendData, setSendData] = useState({
    data: undefined,
    type: null
  });

  const [params, setParams] = useState({
    page: 1,
    pageSize: PAGE_SIZE_LIST
  });

  useEffect(()=>{
    if(!localStorage.getItem('access-token')) return;
    dispatch(getListDevice(params));
  },[])

  useEffect(() => {
    if (statusCreate === STATUS_API.SUCCESS) {
      setIsShowModalDeviceDetails(false)
      dispatch(showToast())
      const newparams = Object.assign({}, params, { page: 0 });
      setParams(newparams);
      dispatch(getListDevice());
    }
  }, [statusCreate]);

  useEffect(() => {
    if (statusDelete === STATUS_API.SUCCESS) {
      dispatch(showToast())
      setParams({page: 1, pageSize: params.pageSize});
      dispatch(getListDevice(params));
    }
  }, [statusDelete]);

  useEffect(() => {
    if (statusUpdate === STATUS_API.SUCCESS) {
      dispatch(showToast())
      setParams({page: 1, pageSize: params.pageSize});
      dispatch(getListDevice(params));
    }
  }, [statusUpdate]);


  const getListDeviceWithParams = data => {
    const paramValue = Object.assign({}, params, data);
    setParams(paramValue);
    dispatch(getListDevice(paramValue));
  };

  const clearSearch = () => {
    const paramValue = {
      page: 1,
      pageSize: PAGE_SIZE_LIST
    };
    // setParams(paramValue);
    // dispatch(GetListDevice(paramValue));
  };

  const showDetailsDevice = data => {
    if (!data) return;
    setSendData(data);
    setIsShowModalDeviceDetails(true);
  };

  const createNewDevice = () => {
    setSendData({ data: {}, type: ACTION_TABLE.CREATE });
    setIsShowModalDeviceDetails(true);
    dispatch(setActiveStep(CREATE_DEVICE_STEP.ADD_INFO_DEVICE));
    dispatch(resetObjectCreating());
  };

  const handleDeleteDevice = data => {
    if (!data) return;
    setDeleteItem(data);
    dispatch(showDialogConfirm());
  };

  const confirmDeleteDevice = () => {
    console.log(deleteItem)
    if (!deleteItem) return;
    setIsDeleted(true);
    dispatch(deleteDevice({id: deleteItem.id}));
  };

  const closeModalDeviceDetails = data => {
    setIsShowModalDeviceDetails(false);
    dispatch(resetChange());
  }

  return (
    <Page className={classes.root} title="Device">

      { statusCreate === STATUS_API.SUCCESS &&
      <ToastMessage type={messageToastType_const.success}
                    message={'Thêm thiết bị thành công'} />
      }

      { statusDelete === STATUS_API.SUCCESS &&
      <ToastMessage type={messageToastType_const.success}
                    message={'Xóa thiết bị thành công'} />
      }
      <Container maxWidth={false}>
        {/* tool bar */}
        <Toolbar
          isLoading={isLoading}
          searchRef={getListDeviceWithParams}
          clearSearchRef={clearSearch}
          createNewDeviceRef={createNewDevice}
          // checkPermissionCreate={checkPermissionEdit}
        />
        {/* result */}
        <Box mt={3}>
          <Results
            // checkPermissionEdit={checkPermissionEdit}
            // checkPermissionView={checkPermissionViewCustomer || checkPermissionViewUser}
            actionDetailsDeviceRef={showDetailsDevice}
            actionDeleteDeviceRef={handleDeleteDevice}
            listDevice={listDevice}
            totalDevice={totalDevice}
            isLoading={isLoading}
            getListDeviceRef={getListDeviceWithParams}
          />
        </Box>
      </Container>
      <DialogConfirm
        title={MESSAGE.CONFIRM_DELETE_DEVICE}
        textOk={MESSAGE.BTN_YES}
        textCancel={MESSAGE.BTN_CANCEL}
        callbackOk={() => confirmDeleteDevice()}
        callbackCancel={() => dispatch(closeDialogConfirm())}
      />

      {isShowModalDeviceDetails && sendData ? (
        <DetailsDevice
          open={isShowModalDeviceDetails}
          sendData={sendData}
          closeRef={closeModalDeviceDetails}
        />
      ) : (
        ''
      )}

      {/*{isChangeDevice && isDeleted ? (*/}
      {/*  <ToastMessage*/}
      {/*    callBack={() => dispatch(resetChange())}*/}
      {/*    message={*/}
      {/*      isChangeDevice == STATUS_API.SUCCESS*/}
      {/*        ? MESSAGE.DELETE_DEVICE_SUCCESS*/}
      {/*        : MESSAGE.DELETE_DEVICE_FAIL*/}
      {/*    }*/}
      {/*    type={*/}
      {/*      isChangeDevice == STATUS_API.SUCCESS*/}
      {/*        ? messageToastType_const.success*/}
      {/*        : messageToastType_const.error*/}
      {/*    }*/}
      {/*  />*/}
      {/*) : (*/}
      {/*  ''*/}
      {/*)}*/}
    </Page>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

export default DeviceListView;

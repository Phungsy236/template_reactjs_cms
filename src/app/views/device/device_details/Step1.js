import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  Card,
  CardContent,
  Divider,
  Grid,
  InputLabel,
  makeStyles,
  TextField,
  Typography
} from '@material-ui/core';
import { Formik } from 'formik';
import * as Yup from 'yup';
import {
  ACTION_TABLE,
  CREATE_DEVICE_STEP,
  messageToastType_const,
  STATUS_API
} from '../../../constant/config';
import CircularProgress from '@material-ui/core/CircularProgress';
import { green } from '@material-ui/core/colors';
import LoadingComponent from '../../../components/Loading';
import { useDispatch, useSelector } from 'react-redux';
import ToastMessage from '../../../components/ToastMessage';
import { resetChange } from '../../../../features/vehicleSlice';
import { MESSAGE } from '../../../constant/message';
import { showToast } from '../../../../features/uiSlice';
import {
  setActiveStep,
  setObjectCreating,
  updateDevice
} from '../../../../features/deviceSlice';

Step1.propTypes = {
  isLoading: PropTypes.bool,
  isEditDevice: PropTypes.bool.isRequired,
  device: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    status: PropTypes.number,
    sim: PropTypes.string,
    mfg: PropTypes.string,
    device_type_id: PropTypes.bool,
    serial: PropTypes.string,
    vehicle_license_plate_exists: PropTypes.bool,
    vehicle_license_plate: PropTypes.string,
    customer_id: PropTypes.string
  }),
  sendDate: PropTypes.object,
  closeRef: PropTypes.func.isRequired
};

function Step1({ isLoading, isEditDevice, sendData, closeRef, handleClose }) {
  // console.log(sendData);
  // console.log(isEditDevice);

  const classes = useStyles();
  const dispatch = useDispatch();
  const [isSubmitted, setSubmit] = React.useState(false);
  const err = useSelector(state => state.deviceSlice.err);
  const statusCreate = useSelector(state => state.deviceSlice.statusCreate);
  const statusUpdate = useSelector(state => state.deviceSlice.statusUpdate);
  const objectCreating = useSelector(state => state.deviceSlice.objectCreating);
  
  console.log(statusUpdate);

  const [device, setDevice] = useState(isEditDevice ? sendData.data : objectCreating);

  const handleSubmit = data => {
    setDevice(data);
    dispatch(setObjectCreating(data));
    if(isEditDevice) {

      const { name, status, serial, sim, mfg } = data

      const payload = {
        id: data.id,
        body: {
          name,
          status,
          serial,
          sim,
          mfg
        }
      }

      dispatch(updateDevice(payload))
      handleClose()
    } else {
      dispatch(setActiveStep(CREATE_DEVICE_STEP.ADD_INFO_VEHICLE));
    }
  };

  useEffect(() => {
    if (
      (statusUpdate && statusUpdate !== STATUS_API.PENDING) ||
      (statusCreate && statusCreate !== STATUS_API.PENDING)
    ) {
      dispatch(showToast());
    }
  }, [statusUpdate || statusCreate]);
  return isLoading ? (
    <LoadingComponent />
  ) : (
    <React.Fragment>
      <Grid container spacing={3}>
        <Grid
          item
          lg={12}
          md={12}
          xs={12}
          style={{
            pointerEvents: isEditDevice ? '' : 'none'
          }}
        >
          <Card className={classes.shadowBox}>
            <CardContent>
              <Formik
                enableReinitialize={true}
                initialValues={{ ...device }}
                validationSchema={Yup.object().shape({
                  name: Yup.string()
                    .max(100)
                    .required('Tên thiết bị không được để trống'),
                  serial: Yup.number()
                    .typeError('Serial phải là dạng số')
                    .required('Serial không được để trống'),
                  sim: Yup.number()
                    .typeError('sim phải là dạng số')
                    .required('Sim không được để trống'),
                  mfg: Yup.number()
                    .typeError('mfg phải là dạng số')
                    .required('Mfg không được để trống')
                })}
                onSubmit={handleSubmit}
              >
                {({
                  errors,
                  handleBlur,
                  handleChange,
                  handleSubmit,
                  touched,
                  values
                }) => (
                  <form onSubmit={handleSubmit}>
                    <Grid container spacing={3}>
                      <Grid item md={12} xs={12}>
                        <InputLabel>Tên thiết bị* :</InputLabel>
                        <TextField
                          error={Boolean(touched.name && errors.name)}
                          fullWidth
                          helperText={touched.name && errors.name}
                          // label="Loại sản phẩm *"
                          margin="normal"
                          name="name"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          value={values.name}
                          variant="outlined"
                        />
                      </Grid>

                      <Grid item md={6} xs={12}>
                        <InputLabel>Serial* :</InputLabel>
                        <TextField
                          error={Boolean(touched.serial && errors.serial)}
                          fullWidth
                          helperText={touched.serial && errors.serial}
                          margin="normal"
                          name="serial"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          value={values.serial}
                          variant="outlined"
                        />
                      </Grid>

                      <Grid item md={6} xs={12}>
                        <InputLabel>Sim* :</InputLabel>
                        <TextField
                          error={Boolean(touched.sim && errors.sim)}
                          fullWidth
                          helperText={touched.sim && errors.sim}
                          // label="Phiên bản"
                          margin="normal"
                          name="sim"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          value={values.sim}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid item md={6} xs={12}>
                        <InputLabel>Mfg* :</InputLabel>
                        <TextField
                          error={Boolean(touched.mfg && errors.mfg)}
                          fullWidth
                          helperText={touched.mfg && errors.mfg}
                          // label="Phiên bản"
                          margin="normal"
                          name="mfg"
                          onBlur={handleBlur}
                          onChange={handleChange}
                          value={values.mfg}
                          variant="outlined"
                        />
                      </Grid>
                    </Grid>
                    {err && isSubmitted && (
                      <Typography
                        color={'error'}
                        variant={'subtitle1'}
                        style={{ marginTop: 30 }}
                      >
                        {err}
                      </Typography>
                    )}
                    <Box my={2} mt={5}>
                      <div className={classes.groupButtonSubmit}>
                        {Boolean(isEditDevice) && (
                          <div className="left-button">
                            <div className={classes.wrapper}>
                              <Button
                                className={classes.styleInputSearch}
                                style={{ marginRight: '10px' }}
                                color="primary"
                                size="large"
                                type="submit"
                                variant="contained"
                              >
                                {isEditDevice == ACTION_TABLE.CREATE
                                  ? 'Đồng Ý'
                                  : 'Cập nhật'}
                              </Button>
                              {isLoading && (
                                <CircularProgress
                                  size={24}
                                  className={classes.buttonProgress}
                                />
                              )}
                            </div>
                            <Button
                              size="large"
                              variant="contained"
                              onClick={handleClose}
                            >
                              Thoát
                            </Button>
                          </div>
                        )}
                      </div>
                    </Box>
                  </form>
                )} 
              </Formik>
            </CardContent>
            <Divider />
          </Card>
        </Grid>
      </Grid>

      {statusUpdate === STATUS_API.SUCCESS ||
        (statusUpdate === STATUS_API.ERROR && (
          <ToastMessage
            callBack={() => dispatch(resetChange())}
            message={
              statusUpdate === STATUS_API.SUCCESS
                ? MESSAGE.UPDATE_DEVICE_SECCESS
                : err
            }
            type={
              statusUpdate === STATUS_API.SUCCESS
                ? messageToastType_const.success
                : messageToastType_const.error
            }
          />
        ))}

      {statusCreate === STATUS_API.SUCCESS ||
        (statusCreate === STATUS_API.ERROR && (
          <ToastMessage
            callBack={() => dispatch(resetChange())}
            message={
              statusCreate === STATUS_API.SUCCESS
                ? MESSAGE.CREATE_DEVICE_SUCCESS
                : err
            }
            type={
              statusCreate === STATUS_API.SUCCESS
                ? messageToastType_const.success
                : messageToastType_const.error
            }
          />
        ))}
    </React.Fragment>
  );
}

export const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative'
  },
  shadowBox: {
    paddingTop: '30px',
    boxShadow: '0 2px 5px rgba(0,0,0,.18)'
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  root: {
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  groupButtonSubmit: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    // justifyItems: 'center',
    marginTop: '10px',
    '& .left-button': {
      display: 'flex'
    }
  },
  avatar: {
    height: 100,
    width: 100
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  wrapper: {
    position: 'relative'
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  },
  disableForm: {
    pointerEvents: 'none'
  },
  colorWhite: {
    color: '#fff'
  }
}));

export default Step1;

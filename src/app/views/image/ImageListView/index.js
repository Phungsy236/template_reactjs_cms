import React, { useEffect, useState } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import Page from 'src/app/components/Page';
import { useDispatch, useSelector } from 'react-redux';
import { GetImages, setPageId } from '../../../../features/imageSlice';
import LoadingComponent from '../../../components/Loading';
import ImageDetail from './ImageDetail';
import HeaderToolBar from './HeaderToolBar';
import { PAGE_SIZE_LIST } from '../../../constant/config';
import NotFoundView from '../../errors/NotFoundView';
import Result from './Result';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  productCard: {
    height: '100%'
  }
}));

const ImageList = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const isLoading = useSelector(state => state.imageSlice.isLoading);
  const err = useSelector(state => state.imageSlice.err);
  const listImages = useSelector(state => state.imageSlice.listImage);
  const currentPageId = useSelector(state => state.imageSlice.currentPageId);
  const totalPage = useSelector(state => state.imageSlice.totalPage);

  const [isShowModalImgDetails, setShowModalImg] = React.useState(false);
  const [sendData, setSendData] = React.useState(undefined);
  const showDetailsImg = data => {
    if (!data) return;
    setSendData(data);
    setShowModalImg(true);
  };

  //----------------- Reload page ---------------------
  const [params, setParams] = useState({
    page: 1,
    page_size: PAGE_SIZE_LIST,
    license_plate: ''
  });

  function handlePageChange(event, value) {
    dispatch(setPageId(value));
    dispatch(GetImages({ page: value, license_plate: '' }));
  }
  const getListImageWithParams = data => {
    const paramValue = Object.assign({}, params, data);
    setParams(paramValue);
    dispatch(GetImages(paramValue));
  };

  const clearSearch = () => {
    const paramValue = {
      page: 1,
      page_size: PAGE_SIZE_LIST,
      license_plate: ''
    };
    setParams(paramValue);
    dispatch(GetImages(paramValue));
  };
  useEffect(() => {
    if (!listImages) dispatch(GetImages(params));
  }, [dispatch, currentPageId]);

  if (err) {
    return (
      <Page className={classes.root} title="Ảnh">
        <Container maxWidth={false}>
          <NotFoundView />
        </Container>
      </Page>
    );
  }
  return (
    <Page className={classes.root} title="VnTech24h - 4G camera">
      <Container maxWidth={false}>
        <React.Fragment>
          <HeaderToolBar
            isLoading={isLoading}
            searchRef={getListImageWithParams}
            clearSearchRef={clearSearch}
          />
          <Box mt={3}>
            <React.Fragment>
              {isLoading ? (
                <LoadingComponent />
              ) : (
                <Result
                  listImages={listImages}
                  actionDetailsImg={showDetailsImg}
                />
              )}
            </React.Fragment>
          </Box>
          {listImages && listImages.length > 0 && (
            <Box mt={3} display="flex" justifyContent="center">
              <Pagination
                color="primary"
                count={totalPage}
                size="small"
                page={currentPageId}
                onChange={handlePageChange}
              />
            </Box>
          )}
        </React.Fragment>
      </Container>
      {isShowModalImgDetails && sendData && (
        <ImageDetail
          open={isShowModalImgDetails}
          data={sendData}
          closeRef={() => setShowModalImg(false)}
        />
      )}
    </Page>
  );
};

export default ImageList;

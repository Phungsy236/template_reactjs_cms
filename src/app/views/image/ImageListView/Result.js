import React from 'react';
import PropTypes from 'prop-types';
import ImageItem from '../../../components/ImageItem';
import { Grid, makeStyles } from '@material-ui/core';
import NullData from '../../../components/NullData';

Result.propTypes = {
  listImages: PropTypes.array.isRequired,
  actionDetailsImgRef: PropTypes.func
};

function Result({ listImages, actionDetailsImg }) {
  const classes = useStyles();
  return (
    listImages && (
      <div>
        {listImages.length === 0 ? (
          <NullData />
        ) : (
          <Grid container spacing={3}>
            {listImages?.map(image => (
              <Grid item key={image.id} lg={4} md={6} xs={12}>
                <ImageItem
                  actionDetailsImgRef={actionDetailsImg}
                  className={classes.productCard}
                  image={image}
                />
              </Grid>
            ))}
          </Grid>
        )}
      </div>
    )
  );
}
const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  productCard: {
    height: '100%'
  }
}));
export default Result;

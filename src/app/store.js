import { configureStore } from '@reduxjs/toolkit';
import authSlice from '../features/authSlice';
import imageSlice from '../features/imageSlice';
import vehicleSlice from '../features/vehicleSlice';
import uiSlice from '../features/uiSlice';
import deviceSlice from '../features/deviceSlice';
import userSlice from '../features/userSlice';

export default configureStore({
  reducer: {
    authSlice: authSlice,
    imageSlice: imageSlice,
    vehicleSlice: vehicleSlice,
    uiSlice: uiSlice,
    deviceSlice: deviceSlice,
    userSlice: userSlice
  }
});

export const MESSAGE = {
  //user
  CHANGE_STATUS_USER_SUCCESS: 'Thay đổi trạng thái tài khoản thành công!',
  CHANGE_STATUS_USER_FAIL: 'Thay đổi trạng thái tài khoản thất bại!',
  CHANGE_ROLE_USER_SUCCESS: 'Thay đổi quyền hạn tài khoản thành công!',
  CHANGE_ROLE_USER_FAIL: 'Thay đổi quyền hạn tài khoản thất bại!',

  UPDATE_USER_SUCCESS: 'Cập nhật tài khoản thành công!',
  UPDATE_USER_FAIL: 'Cập nhật tài khoản thất bại!',
  CREATE_USER_FAIL: 'Tạo mới tài khoản thất bại!',
  CREATE_USER_SUCCESS: 'Tạo mới tài khoản thành công!',

  // device
  CONFIRM_DELETE_DEVICE: 'Bạn có muốn xóa thiết bị này?',
  DELETE_DEVICE_SUCCESS: 'Xóa thiết bị thành công.',
  DELETE_DEVICE_FAIL: 'Xóa thiết bị thất bại',
  UPDATE_DEVICE_SUCCESS: 'Cập nhật thiết bị thành công',
  CREATE_DEVICE_SUCCESS: 'Tạo mới thiết bị thành công',

  // commom
  BTN_YES: 'Đồng ý',
  BTN_CANCEL: 'Hủy',

  // vehicle
  CONFIRM_DELETE_VEHICLE: 'Bạn có muốn xóa phương tiện này?',
  DELETE_VEHICLE_SUCCESS: 'Xóa phương tiện thành công.',
  DELETE_VEHICLE_FAIL: 'Xóa phương tiện thất bại',
  UPDATE_VEHICLE_SECCESS: 'Cập nhật phương tiện thành công',
  CREATE_VEHICLE_SECCESS: 'Tạo mới phương tiện thành công'
};

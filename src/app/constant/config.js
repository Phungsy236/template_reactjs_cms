import * as Yup from 'yup';

export const PAGE_SIZE = 9;

export const PAGE_SIZE_LIST = 10;

export const messageToastType_const = {
  error: 'error',
  warning: 'warning',
  info: 'info',
  success: 'success'
};

export const roleName_const = [
  'Giám đốc',
  'Quản lý',
  'Kế toán',
  'Nhân viên Sale',
  'Đại lý',
  'Khách hàng'
];

export const userShape_const = {
  email: Yup.string()
    .email('Email chưa đúng định dạng')
    .max(255)
    .required('Bạn cần nhập Email'),
  full_name: Yup.string()
    .min(2, 'Tên quá ngắn')
    .max(50, 'Tên quá dài')
    .required('Bạn cần nhập đầy đủ họ tên'),
  phone: Yup.string()
    .matches(new RegExp('^[0-9-+]{9,11}$'), 'Số điện thoại phải đúng định dạng')
    .required('Bạn cần nhập số điện thoại'),
  password: Yup.string()
    .min(4, 'Quá ngắn')
    .max(20, 'Quá dài')
    .required('Bạn cần nhập mật khẩu'),
  address: Yup.string()
    .max(100, 'Quá dài')
    .required('Bạn cần điền địa chỉ')
  // dob: Yup.date().required('Bạn cần chọn ngày sinh')
};

export const DRAFT_USER = { id: 3, value: 'Bản Nháp' };

export const STATUS_API = {
  PENDING: 0,
  SUCCESS: 1,
  ERROR: 2
};

export const DEVICE_STATUS_VALUE = [
  { id: 1, title: 'Lưu kho' },
  { id: 2, title: 'Bảo hành' }
];

export const DEVICE_STATUS = {
  STORAGE: 1,
  GUARANTEE: 2
};

export const ACTION_TABLE = {
  CREATE: 'create',
  EDIT: 'edit',
  PREVIEW: 'preview',
  DELETE: 'delete'
};

export const MEDIA_VIEW_TYPE = {
  IMAGES: 0,
  VIDEOS: 1
};
export const HTTP_GETTYPE = {
  ALL: 0,
  ALL_PAGINATION: 1,
  DETAIL: 2
};
export const TIMEOUT_DEFAULT = 5000;
export const MSG_TIMEOUT_REQUEST = 'Server phản hồi quá lâu , vui lòng thử lại';
export const CREATE_DEVICE_STEP = {
  ADD_INFO_DEVICE: 1,
  ADD_INFO_VEHICLE: 2
};

export const VEHICLE_STATUS = {
  ACTIVE: 'active',
  NOT_ACTIVE: 'not_active'
}
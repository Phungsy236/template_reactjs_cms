import React from 'react';
import { Navigate } from 'react-router-dom';
import DashboardLayout from 'src/app/layouts/DashboardLayout';
import MainLayout from 'src/app/layouts/MainLayout';
import LoginView from 'src/app/views/auth/LoginView';
import RegisterView from 'src/app/views/auth/RegisterView';
import NotFoundView from './app/views/errors/NotFoundView';
import DraftPageView from './app/views/errors/DraftPageView';
import Camera from './app/views/streamCamera';
import DeviceListView from './app/views/device';
import ImageListView from './app/views/image/ImageListView';
import VehicleListView from './app/views/vehicle';
import MapView from './app/views/map';
import UserListView from './app/views/agency';
import AgencyListView from './app/views/agency';
import CreateAgency from './app/views/agency/agency_create';
import CreateVehicle from './app/views/vehicle/vehicle_create';
const routes = [
  {
    path: 'app',
    element: <DashboardLayout />,
    children: [
      // { path: 'p2p', element: <Camera /> },
      // { path: 'images', element: <ImageListView /> },
      { path: 'device', element: <DeviceListView /> },
      { path: 'map', element: <MapView /> },
      {
        path: 'vehicle',
        children: [
          { path: 'all', element: <VehicleListView /> },
          { path: 'images', element: <ImageListView /> },
          { path: 'camera', element: <Camera /> },
          { path: 'create', element: <CreateVehicle /> }
        ]
      },
      {
        path: 'user',
        children: [
          { path: 'agencies', element: <AgencyListView /> },
          { path: 'create', element: <CreateAgency /> }
        ]
      },
      { path: '*', element: <Navigate to="/app/vehicle/all" /> }
    ]
  },
  {
    path: '/',
    element: <MainLayout />,
    children: [
      { path: 'login', element: <LoginView /> },
      { path: 'register', element: <RegisterView /> },
      { path: '404', element: <NotFoundView /> },
      { path: 'waiting-confirm', element: <DraftPageView /> },
      { path: '/', element: <Navigate to="/app/p2p?role=viewer" /> },
      { path: '*', element: <Navigate to="/404" /> }
    ]
  }
];

export default routes;
